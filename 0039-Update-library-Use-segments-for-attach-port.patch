From 6a07c4da113dcf3609f31836d140f7ea4ad0d131 Mon Sep 17 00:00:00 2001
From: Daniel Failing <daniel.failing@cern.ch>
Date: Thu, 23 Nov 2023 14:09:15 +0100
Subject: [PATCH] Attach Interface: Add flag to use segments for the
 cluster_name instead of network cluster

---
 nova/cern/neutron.py    |  3 +++
 nova/cern/utils.py      | 21 ++++++++++++++++-----
 nova/conf/cern.py       |  4 ++++
 nova/network/neutron.py |  3 +++
 4 files changed, 26 insertions(+), 5 deletions(-)

diff --git a/nova/cern/neutron.py b/nova/cern/neutron.py
index baa9712c51..448a96724e 100644
--- a/nova/cern/neutron.py
+++ b/nova/cern/neutron.py
@@ -23,6 +23,9 @@ class Neutron(object):
     def show_subnet(self, subnet_id):
         return self.network_api.show_subnet(self.context, subnet_id)
 
+    def show_segment(self, segment_id):
+        return self.network_api.show_segment(self.context, segment_id)
+
     def list_clusters(self):
         return self.network_api.list_clusters(self.context)
 
diff --git a/nova/cern/utils.py b/nova/cern/utils.py
index 9741a39597..ae748f2bfa 100644
--- a/nova/cern/utils.py
+++ b/nova/cern/utils.py
@@ -65,6 +65,7 @@ def attach_interface(network_api, context, instance, port_id):
                 "Not allowed to attach an interface without"
                 "fixed ips")
 
+        segment_uuid = ""
         for fixed_ip in port_info["port"]["fixed_ips"]:
             subnet_id = fixed_ip["subnet_id"]
             subnet_info = neutron.show_subnet(subnet_id)
@@ -78,6 +79,12 @@ def attach_interface(network_api, context, instance, port_id):
             else:
                 ipv6s.append(address)
 
+            if "segment_id" in subnet_info["subnet"]:
+                other = subnet_info["subnet"]["segment_id"]
+                if len(segment_uuid) > 1 and segment_uuid != other:
+                    LOG.warning("Port has multiple segments.")
+                segment_uuid = other
+
         # Check that we have a single subnet
         if len(set(subnet_names)) > 1:
             raise Exception(
@@ -96,12 +103,16 @@ def attach_interface(network_api, context, instance, port_id):
                 "Not allowed to have multiple ip v6 addresses on "
                 "single interface")
 
-        cluster_info = neutron.list_clusters()
+        if CONF.cern.utils_attach_use_segments_instead_of_netcluster:
+            segment_info = neutron.show_segment(segment_uuid)
+            cluster_name = segment_info["segment"]["name"]
+        else:
+            cluster_info = neutron.list_clusters()
 
-        for cluster in cluster_info["clusters"]:
-            if subnet_id in cluster["subnets"]:
-                cluster_name = cluster["name"]
-                break
+            for cluster in cluster_info["clusters"]:
+                if subnet_id in cluster["subnets"]:
+                    cluster_name = cluster["name"]
+                    break
 
         subnet_name = subnet_names[0]
         ipv4 = ipv4s[0] if len(ipv4s) > 0 else None
diff --git a/nova/conf/cern.py b/nova/conf/cern.py
index dccdc78536..1e09213b7c 100644
--- a/nova/conf/cern.py
+++ b/nova/conf/cern.py
@@ -46,6 +46,10 @@ cern_opts = [
     cfg.BoolOpt('utils_attach_interface',
         default=False,
         help="Triggers the integration with landb on attachment"),
+    cfg.BoolOpt('utils_attach_use_segments_instead_of_netcluster',
+        default=False,
+        help="When registering an interface in LanDB query for the VMPOOL"
+        " using segments instead of netcluster API"),
     cfg.BoolOpt('utils_detach_interface',
         default=False,
         help="Triggers the integration with landb on detachment"),
diff --git a/nova/network/neutron.py b/nova/network/neutron.py
index dd41d36699..ba6a85e310 100644
--- a/nova/network/neutron.py
+++ b/nova/network/neutron.py
@@ -1985,6 +1985,9 @@ class API:
     def show_subnet(self, context, subnet_id):
         return get_client(context, admin=True).show_subnet(subnet_id)
 
+    def show_segment(self, context, segment_uuid):
+        return get_client(context, admin=True).show_segment(segment_uuid)
+
     def list_clusters(self, context):
         return get_client(context, admin=True).list_clusters()
 
-- 
2.42.1

